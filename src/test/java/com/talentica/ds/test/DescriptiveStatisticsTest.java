/**
 * 
 */
package com.talentica.ds.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Assert;
import org.junit.Test;

import com.talentica.ds.DescriptiveStatisticsDouble;
import com.talentica.ds.DescriptiveStatisticsInteger;
import com.talentica.ds.DescriptiveStatisticsString;

/**
 * @author pooshans
 *
 */
public class DescriptiveStatisticsTest {

	private static final double DELTA = 1e-15;

	@Test
	public void testSingle() {
		DescriptiveStatisticsInteger<Integer> descriptiveStatistics = new DescriptiveStatisticsInteger<Integer>(
				new Integer[] { 1 });
		Assert.assertEquals(1.0, descriptiveStatistics.getPercentile(50), DELTA);
	}

	@Test
	public void testTwoOnly() {
		DescriptiveStatisticsInteger<Integer> descriptiveStatistics = new DescriptiveStatisticsInteger<Integer>(
				new Integer[] { 5, 7 });
		Assert.assertEquals(6.0, descriptiveStatistics.getPercentile(50), DELTA);
	}

	@Test
	public void testOddWithDuplicate() {
		DescriptiveStatisticsInteger<Integer> descriptiveStatistics = new DescriptiveStatisticsInteger<Integer>(
				new Integer[] { 5, 4, 3, 2, 1, 4, 3 });
		Assert.assertEquals(3, descriptiveStatistics.getPercentile(50), DELTA);
	}

	@Test
	public void testOddWithoutDuplicate() {
		DescriptiveStatisticsInteger<Integer> descriptiveStatistics = new DescriptiveStatisticsInteger<Integer>(
				new Integer[] { 5, 4, 3, 2, 1 });
		Assert.assertEquals(3, descriptiveStatistics.getPercentile(50), DELTA);
	}

	@Test
	public void testEvenWithDuplicate() {
		DescriptiveStatisticsInteger<Integer> descriptiveStatistics = new DescriptiveStatisticsInteger<Integer>(
				new Integer[] { 5, 2, 5, 5, 2, 3 });
		Assert.assertEquals(4.0, descriptiveStatistics.getPercentile(50), DELTA);
	}

	@Test
	public void testEvenWithoutDuplicate() {
		DescriptiveStatisticsInteger<Integer> descriptiveStatistics = new DescriptiveStatisticsInteger<Integer>(
				new Integer[] { 10, 100, 45, 1, 8, 15 });
		Assert.assertEquals(12.5, descriptiveStatistics.getPercentile(50), DELTA);
	}

	@Test
	public void testFirstRandom() {
		DescriptiveStatisticsInteger<Integer> descriptiveStatistics = new DescriptiveStatisticsInteger<Integer>(
				new Integer[] { 100, 500, 65, 190, 234, 89, 1, -1, 56, 80, 100, 3 });
		Assert.assertEquals(84.5, descriptiveStatistics.getPercentile(50), DELTA);
	}

	@Test
	public void testSecondRandom() {
		DescriptiveStatisticsInteger<Integer> descriptiveStatistics = new DescriptiveStatisticsInteger<Integer>(
				new Integer[] { 1, 2, 50, 3, 4, 30, 2, 3 });
		Assert.assertEquals(3, descriptiveStatistics.getPercentile(50), DELTA);
	}

	@Test
	public void testThirdRandom() {
		DescriptiveStatisticsInteger<Integer> descriptiveStatistics = new DescriptiveStatisticsInteger<Integer>(
				new Integer[] { -1, -2, -50, -3, -4, -30, -2, -3, -2, 10 });
		Assert.assertEquals(-2.5, descriptiveStatistics.getPercentile(50), DELTA);
	}

	@Test
	public void testFourthRandom() {
		DescriptiveStatisticsInteger<Integer> descriptiveStatistics = new DescriptiveStatisticsInteger<Integer>(
				new Integer[] { 100, 300, 200, 500, 100, 400, 600, 499, 240 });
		Assert.assertEquals(300, descriptiveStatistics.getPercentile(50), DELTA);
	}

	@Test
	public void testFifthRandom() {
		DescriptiveStatisticsInteger<Integer> descriptiveStatistics = new DescriptiveStatisticsInteger<Integer>(
				new Integer[] { 5, 29, 10, 33, 33, 33, 6, 7, 8, 10 });
		Assert.assertEquals(10, descriptiveStatistics.getPercentile(50), DELTA);
	}

	@Test
	public void testSixthRandom() {
		DescriptiveStatisticsDouble<Double> descriptiveStatistics = new DescriptiveStatisticsDouble<Double>(
				new Double[] { 5.0, 29.4, 5.67, 10.99, 33.34, 33.90, 33.38, 6.00, 7.24, 8.88 });
		Assert.assertEquals(9.935, descriptiveStatistics.getPercentile(50), DELTA);
	}
	
	@Test
	public void testFifthWithAddSequencially() {
		Integer[] keys = new Integer[] { 100, 300, 200, 500, 100, 400, 600, 499, 240 };
		DescriptiveStatisticsDouble<Double> descriptiveStatistics = new DescriptiveStatisticsDouble<Double>();
		for (int key : keys) {
			descriptiveStatistics.add(key);
		}
		Assert.assertEquals(300, descriptiveStatistics.getPercentile(50), DELTA);
	}

	@Test
	public void testUnique() {
		DescriptiveStatisticsInteger<Integer> descriptiveStatistics = new DescriptiveStatisticsInteger<Integer>(
				new Integer[] { 5, 29, 5, 33, 33, 33, 6, 10, 7, 8, 10 });
		Assert.assertEquals(7, descriptiveStatistics.unique(), DELTA);
	}

	@Test
	public void testOddString() {
		DescriptiveStatisticsString<String> descriptiveStatistics = new DescriptiveStatisticsString<String>(
				new String[] { "A", "S", "P", "E", "L" });
		Assert.assertArrayEquals(new String[] { "L" }, descriptiveStatistics.getPercentile(50));
	}

	@Test
	public void testEvenString() {
		DescriptiveStatisticsString<String> descriptiveStatistics = new DescriptiveStatisticsString<String>(
				new String[] { "A", "B", "C", "D" });
		Assert.assertArrayEquals(new String[] { "B", "C" }, descriptiveStatistics.getPercentile(50));
	}

	public static void main(String[] args) throws IOException {
		File file = new File("/home/pooshans/hh/test.csv");
		int keyIndex = 0;
		int valueIndex = 6;
		Reader fis = new FileReader(file);
		BufferedReader br = new BufferedReader(fis);
		Map<String, DescriptiveStatisticsDouble<Double>> medianMap = new HashMap<>();
		String line;
		long startTime = System.currentTimeMillis();
		while ((line = br.readLine()) != null) {
			String[] token = line.split(",");
			String key = token[keyIndex];
			DescriptiveStatisticsDouble<Double> descriptiveStatisticsNumber = medianMap.get(key);
			if (descriptiveStatisticsNumber == null) {
				descriptiveStatisticsNumber = new DescriptiveStatisticsDouble<Double>();
				medianMap.put(key, descriptiveStatisticsNumber);
			}
			descriptiveStatisticsNumber.add(Double.valueOf(token[valueIndex]));
		}
		for (Entry<String, DescriptiveStatisticsDouble<Double>> statisticsNumberEntry : medianMap.entrySet()) {
			System.out.println("Key :: " + statisticsNumberEntry.getKey() + " median :: "
					+ statisticsNumberEntry.getValue().getPercentile(50));
		}
		System.out.println("Total time taken :: " + (System.currentTimeMillis() - startTime));
	}

}
