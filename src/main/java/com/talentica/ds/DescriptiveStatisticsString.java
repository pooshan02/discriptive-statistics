/**
 * 
 */
package com.talentica.ds;

/**
 * @author pooshans
 *
 */
public class DescriptiveStatisticsString<T extends String> extends AbstractDescriptiveStatistics {

	private int percentile;

	public <T extends Number> DescriptiveStatisticsString() {
	}

	public <T extends Number> DescriptiveStatisticsString(Comparable<String>[] keys) {
		super(keys);
	}

	public Comparable[] getPercentile(int percentile) {
		return calculate(percentile);
	}

	@Override
	public Comparable[] calculate(int percentile) {
		if (percentile <= 0 || percentile > 100) {
			throw new IllegalArgumentException("Percentile should be > 0 and <= 100");
		}
		this.percentile = percentile;
		return traverseTree(root, false);
	}

	/**
	 * @param n
	 *            : It is the root of the AVL tree form where the actual
	 *            traversal start for median search.
	 * @param midPointFound
	 *            : It the identifier which signal that one key is found in mid
	 *            point and start looking for other one.
	 * @return It returns the median.
	 */
	private String[] traverseTree(Node n, boolean midPointFound) {
		if (n == null) {
			return new String[] { "" };
		} else {
			int leftKeyCount = childKeyCount(n.left);
			int rightKeyCount = childKeyCount(n.right);

			if (leftKeyCount == 0 && rightKeyCount == 0) {
				if (totalCount % 2 == 0 && !midPointFound) {
					if (n.keyCount == 1) {
						/* no duplicate at current node */
						return new String[] { n.key.toString(), n.parent.key.toString() };
					} else {
						/*
						 * mid point found at current node and it's duplicate
						 * value, simply return it.
						 */
						return new String[] { n.key.toString() };
					}
				} else {
					return new String[] { n.key.toString() };
				}
			}

			if ((percentile) * (n.keyCount + leftKeyCount + n.leftCarry) < (100 - percentile)
					* (rightKeyCount + n.rightCarry)) {
				if (n.right != null) {
					n.right.leftCarry = n.keyCount + leftKeyCount + n.leftCarry;
					n.right.rightCarry = n.rightCarry;
					n.right.parent = n;
				} else {
					if (n.left != null) {
						return new String[] { n.key.toString(), n.left.key.toString() };
					}
				}
				return traverseTree(n.right, midPointFound);
			} else if ((percentile) * (n.keyCount + rightKeyCount + n.rightCarry) < (100 - percentile)
					* (leftKeyCount + n.leftCarry)) {
				if (n.left != null) {
					n.left.rightCarry = n.keyCount + rightKeyCount + n.rightCarry;
					n.left.leftCarry = n.leftCarry;
					n.left.parent = n;
				} else {
					if (n.right != null) {
						return new String[] { n.key.toString(), n.right.key.toString() };
					}
				}
				return traverseTree(n.left, midPointFound);
			} else if ((percentile) * (n.keyCount + leftKeyCount + n.leftCarry) == (100 - percentile)
					* (rightKeyCount + n.rightCarry)) {
				if (n.right != null) {
					n.right.leftCarry = n.keyCount + leftKeyCount + n.leftCarry;
					n.right.rightCarry = n.rightCarry;
					n.right.parent = n;
				} else {
					if (n.left != null) {
						return new String[] { n.key.toString(), n.left.key.toString() };
					}
				}
				return new String[] { n.key.toString(), traverseTree(n.right, true)[0] };
			} else if ((percentile) * (n.keyCount + rightKeyCount + n.rightCarry) == (100 - percentile)
					* (leftKeyCount + n.leftCarry)) {
				if (n.left != null) {
					n.left.rightCarry = n.keyCount + rightKeyCount + n.rightCarry;
					n.left.leftCarry = n.leftCarry;
					n.left.parent = n;
				} else {
					if (n.right != null) {
						return new String[] { n.key.toString(), n.right.key.toString() };
					}
				}
				return new String[] { n.key.toString(), traverseTree(n.left, true)[0] };
			} else {
				return new String[] { n.key.toString() };
			}
		}
	}

}
