/**
 * 
 */
package com.talentica.ds;

/**
 * @author pooshans
 *
 */
public class DescriptiveStatisticsDouble<T extends Double> extends AbstractDescriptiveStatisticsNumber<T> {

	public <T extends Double> DescriptiveStatisticsDouble() {
	}

	public <T extends Integer> DescriptiveStatisticsDouble(Double[] keys) {
		super(keys);
	}

	@Override
	public double getPercentile(int percentile) {
		return (double) calculate(percentile)[0];
	}

}
