/**
 * 
 */
package com.talentica.ds;

/**
 * @author pooshans
 *
 */
public class DescriptiveStatisticsInteger<T extends Integer> extends AbstractDescriptiveStatisticsNumber<T> {

	public <T extends Integer> DescriptiveStatisticsInteger() {
	}

	public <T extends Integer> DescriptiveStatisticsInteger(Integer[] keys) {
		super(keys);
	}

	@Override
	public double getPercentile(int percentile) {
		return (double) calculate(percentile)[0];
	}

}
